#include <iostream>
#include <opencv2/opencv.hpp>
#include <time.h>

using namespace std;
using namespace cv;

class Vector2d
{
public:
	int x;
	int y;
};
 
class Circle
{
public:
	Vector2d center;
	unsigned int r;						
};

class Box
{
public:
	Vector2d position;					
 
	unsigned int width;					
	unsigned int height;	
};

// sprawdzam czy punkt lezy w środku okręgu
//1. liczę długośc punktu p z punktem srodka okregu
//2. jeżeli dlugośc mniejsza lub równa wtedy punkt znajduje się w środku
bool isCollision(Vector2d p, Circle circle)
{
	//float d = sqrt(pow(p.x - circle.center.x, 2) + sqrt(pow(p.y - circle.center.y, 2)));
	float d = sqrt( pow(p.x - circle.center.x, 2) + pow(p.y - circle.center.y, 2));
 
	if (d <= circle.r)
		return true;
	else
		return false;
}

bool isCollision(Vector2d p, Box box)
{
	if (p.x >= box.position.x && p.x <= box.position.x + box.width && p.y >= box.position.y && p.y <= box.position.y + box.height)
		return true;
	else
		return false;
}

void tocz(int velocity, Circle ball) {
	
		ball.center.x+=velocity;
		ball.center.y+=velocity;
	
	}

int main(int argc, char *argv[])
{
	srand ( time(NULL) );
	const string ORIGINAL_WINDOW = "Kamerka"; 
	string THRESHOLD_WINDOW = "Progi";
	
	Mat frame, original_img, hsv_img, threshold_img, contour;
	
	cout << "Inicjalizuję program >:D." << endl;
	VideoCapture camera(0);
	
	// definicja rozmiaru okna
	int window_size_x = camera.get(CV_CAP_PROP_FRAME_WIDTH);
	int window_size_y =	camera.get(CV_CAP_PROP_FRAME_HEIGHT);
	
	cout << "Rozmiar okna to " << window_size_x << "x" << window_size_y << endl;
	
	// standardowe zabezpieczenie
	if(!camera.isOpened()) {
		cout << "Nie wykryto kamerki :(." << endl;
		return 0;
		} else
		cout << "Kamerka wykryta! :P" << endl;
	
	//zainicjowanie wartosci dla thresholda	
	int minHue = 43;
	int maxHue = 179;
	
	int minSat = 40;
	int maxSat = 255;
	
	int minValue = 107;
	int maxValue = 244;
	
	// nazwanie okienek	
	namedWindow(ORIGINAL_WINDOW, CV_WINDOW_AUTOSIZE);
	namedWindow(THRESHOLD_WINDOW, CV_WINDOW_AUTOSIZE);
	namedWindow("Gra", CV_WINDOW_AUTOSIZE);
	//namedWindow("Kontury", CV_WINDOW_AUTOSIZE);
	
	//kontrola hue
	cvCreateTrackbar("Minimum Hue", "Progi", &minHue, 179);
	cvCreateTrackbar("Maximum Hue", "Progi", &maxHue, 179); 
	
	//kontrola saturation
	cvCreateTrackbar("Minimum Saturation", "Progi", &minSat, 255); 
	cvCreateTrackbar("Maximum Saturation", "Progi", &maxSat, 255); 
	
	//kontrola value
	cvCreateTrackbar("Minimum Value", "Progi", &minValue, 255); 
	cvCreateTrackbar("Maximum Value", "Progi", &maxValue, 255); 
	
	// scoreboard
	int wynik_lewy = 0;
	int wynik_prawy = 0;
	
	// pileczka na boisku
	Circle ball;
		ball.r = 10;
		ball.center.x = window_size_x/2; // umieszczenie poczatkowo pilki na srodku
		ball.center.y = window_size_y/2;
	
	Box bramka_lewa;
		bramka_lewa.position.x = 0;
		bramka_lewa.width = 13;
		bramka_lewa.height = 40;
		bramka_lewa.position.y = window_size_y/2-bramka_lewa.height/2;
		
	Box bramka_prawa;
		bramka_prawa.width = 13;
		bramka_prawa.position.x = window_size_x-bramka_prawa.width - 1;
		bramka_prawa.height = 40;
		bramka_prawa.position.y = window_size_y/2-bramka_prawa.height/2;
	
	// stworzenie kontrolera ~ jest to okrąg lecz zostaną wyliczone na jego podstawie 8 punktów. W zależności od rodzaju styku punktu z okręgiem zostanie wyznaczony kierunek odbicia.
    Circle kontroler;
    int kontroler_r_fixed = 15;
    int velocity = 5;
    int tab[8] = {1,2,3,4,5,6,7,8};
    int direction = direction = tab[rand() % 8];
	
	while(1) {
	
		//pobieranie klatek
		camera >> frame;	
		frame.copyTo(original_img); //skopiowanie orignalu do mata	
		Mat bramki = Mat::zeros( frame.size(), CV_8UC3 ); // stworzenie pustego okienka na ktorym bedzie rysowana gra
		cvtColor(original_img,hsv_img,COLOR_BGR2HSV); //konwersja z BGR na HSV
		inRange(hsv_img,Scalar(minHue,minSat,minValue),Scalar(maxHue,maxSat,maxValue),threshold_img); // progowanie kolorów
		
		//filtrowanie dla progowania ~~EKSPERYMENTALNE~~
		erode(threshold_img, threshold_img, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
		dilate(threshold_img, threshold_img, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) ); 
	
		dilate(threshold_img, threshold_img, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) ); 
		erode(threshold_img, threshold_img, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)) );
		blur(threshold_img, threshold_img, cv::Size(3,3));
		
		///////////////////
		
		// miejsce na kontury obiektu
		vector<vector<Point> > contours; 
        vector<Point> contours_poly; 
        
        // opakowanie obiektu w prostokąt
        Rect boundRect; 
        threshold_img.copyTo(contour); 
        
        // znaleznienie kontur
        findContours( contour, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) ); 
        int max = 0, i_cont = -1; 
        Mat drawing = Mat::zeros( contour.size(), CV_8UC3 ); 
        for( int i = 0; i< contours.size(); i++ ) 
        { 
            if ( abs(contourArea(Mat(contours[i]))) > max ) 
            { 
                max = abs(contourArea(Mat(contours[i]))); 
                i_cont = i; 
            } 
        } 
        Vector2d b_p1;
			b_p1.x = ball.center.x - ball.r;
			b_p1.y = ball.center.y - ball.r;
        Vector2d b_p2;
			b_p2.x = ball.center.x;
			b_p2.y = ball.center.y - ball.r;
        Vector2d b_p3;
			b_p3.x = ball.center.x + ball.r;
			b_p3.y = ball.center.y - ball.r;
        Vector2d b_p4;
        	b_p4.x = ball.center.x - ball.r;
			b_p4.y = ball.center.y;
        Vector2d b_p5;
        	b_p5.x = ball.center.x + ball.r;
			b_p5.y = ball.center.y;
        Vector2d b_p6;
        	b_p6.x = ball.center.x - ball.r;
			b_p6.y = ball.center.y + ball.r;
        Vector2d b_p7;
        	b_p7.x = ball.center.x;
			b_p7.y = ball.center.y + ball.r;
        Vector2d b_p8;
        	b_p8.x = ball.center.x + ball.r;
			b_p8.y = ball.center.y + ball.r;

        
        if ( i_cont >= 0 ) 
        {
									approxPolyDP( Mat(contours[i_cont]), contours_poly, 3, true ); 
									boundRect = boundingRect( Mat(contours_poly) ); 
									rectangle(original_img, boundRect.tl(), boundRect.br(), Scalar(125, 250, 125), 2, 8, 0 ); 
									
									kontroler.center.x = boundRect.x + boundRect.width/2;
									kontroler.center.y = boundRect.y + boundRect.height/2;
									kontroler.r =  15;
									
											/* punktowanie kontrolera
								 * 
								 * .		.		.
								 * p1		p2		p3
								 * 
								 * .		._______.
								 * p4		s	r	p5
								 * 
								 * .		.		.
								 * p6		p7		p8
								 * 
								 * */
							   
								// kolizja z kontrolerem
								
								if(isCollision(b_p1, kontroler)) {	
									direction = 1;
								} else
								
								if(isCollision(b_p2, kontroler)) {	
									direction = 2;
								} else

								if(isCollision(b_p3, kontroler)) {	
									direction = 3;
								} else

								if(isCollision(b_p4, kontroler)) {	
									direction = 4;
								} else
								
								if(isCollision(b_p5, kontroler)) {	
									direction = 5;
								} else
								
								if(isCollision(b_p6, kontroler)) {	
									direction = 6;
								} else
								
								if(isCollision(b_p7, kontroler)) {	
									direction = 7;
								} else
								
								if(isCollision(b_p8, kontroler)) {	
									direction = 8;
								}
								
								
								//                    
									//rysowanie kontrolera   
									circle(original_img, Point(kontroler.center.x,kontroler.center.y), floor((boundRect.width/2+boundRect.height/2)/2), Scalar(255,255,255), 2, 8, 0); // proporcjonalny do rozmiaru obiektu
									circle(bramki, Point(kontroler.center.x,kontroler.center.y), kontroler.r, Scalar(0,255,0), -1, 8, 0); // fixed - wlasciwy do gry
											   
									// informacje
									
									string srodek_kontrolera, promien_kontrolera; 
									stringstream srodek_kontrolera_s, promien_kontrolera_s;
									 
									// rozmiar obiektu + jego promien 
									promien_kontrolera_s << boundRect.width << "x" << boundRect.height << ", " << kontroler.r;
									promien_kontrolera = promien_kontrolera_s.str();
									
									// srodek kontrolera
									srodek_kontrolera_s << boundRect.x + boundRect.width/2 << "x" << boundRect.y + boundRect.height/2;  
									srodek_kontrolera = srodek_kontrolera_s.str(); 
									
									putText( original_img, srodek_kontrolera, Point(50, 50), CV_FONT_HERSHEY_COMPLEX, 0.6, Scalar(130, 0, 130), 2, 8 ); 
									putText( original_img, promien_kontrolera, Point(50, 80), CV_FONT_HERSHEY_COMPLEX, 0.6, Scalar(255, 255, 80), 2, 8 ); 
									
									putText( bramki, "Player", Point(kontroler.center.x-25,kontroler.center.y+30), CV_FONT_HERSHEY_COMPLEX, 0.5, Scalar(0, 255, 0), 2, 8 );
											
        } 
        
        
			if(direction == 0) {ball.center.x+=velocity;} // stoi
			if(direction == 1) {ball.center.x+=velocity;
								ball.center.y+=velocity;} // dol-prawo
			if(direction == 2) {ball.center.y+=velocity;} // dol
			if(direction == 3) {ball.center.x-=velocity;
								ball.center.y+=velocity;} // dol-lewo
			if(direction == 4) {ball.center.x+=velocity;} // prawo
			if(direction == 5) {ball.center.x-=velocity;} // lewo
			if(direction == 6) {ball.center.x+=velocity;
								ball.center.y-=velocity;} // gora-prawogit 
			if(direction == 7) {ball.center.y-=velocity;} // gore
			if(direction == 8) {ball.center.x-=velocity;
								ball.center.y-=velocity;} // gora-lewo
			
        
        		// kolizja z bramkami
		
			if(isCollision(b_p3, bramka_prawa) || isCollision(b_p5, bramka_prawa) || isCollision(b_p8, bramka_prawa)) {	
				ball.center.x = window_size_x/2;
				ball.center.y = window_size_y/2;
				wynik_lewy++;
			}
			
			if(isCollision(b_p1, bramka_lewa) || isCollision(b_p4, bramka_lewa) || isCollision(b_p6, bramka_lewa)) {	
				ball.center.x = window_size_x/2;
				ball.center.y = window_size_y/2;
				wynik_prawy++;
			}
			
			// kolizja ze sciana
			
			int tab_dol[3] = {6,7,8};
			int tab_prawo[3] = {3,5,8};
			int tab_lewo[3] = {1,4,6};
			int tab_gora[3] = {1,2,3};
			
			// dol, mozliwe 6,7,8
			if(b_p7.y >= window_size_y) {
				direction = tab_dol[rand() % 3];
				}
				
			// prawo, mozliwe 3,5,8
		    if(b_p5.x >= window_size_x) {
				direction = tab_prawo[rand() % 3];
				}	
			
			// lewo, mozliwe 1,4,6
			if(b_p4.x <= 0) {
				direction = tab_lewo[rand() % 3];
				}
			
			// gora	, mozliwe 1,2,3
			if(b_p2.y <= 0) {
				direction = tab_gora[rand() % 3];
				}
        
		/* punkty kontrolne
        		circle(bramki, Point(b_p1.x,b_p1.y), 3, Scalar(0,0,255), -1, 8, 0);
				circle(bramki, Point(b_p2.x,b_p2.y), 3, Scalar(0,0,255), -1, 8, 0);
				circle(bramki, Point(b_p3.x,b_p3.y), 3, Scalar(0,0,255), -1, 8, 0);
				circle(bramki, Point(b_p4.x,b_p4.y), 3, Scalar(0,0,255), -1, 8, 0);
				circle(bramki, Point(b_p5.x,b_p5.y), 3, Scalar(0,0,255), -1, 8, 0);
				circle(bramki, Point(b_p6.x,b_p6.y), 3, Scalar(0,0,255), -1, 8, 0);
				circle(bramki, Point(b_p7.x,b_p7.y), 3, Scalar(0,0,255), -1, 8, 0); 
				circle(bramki, Point(b_p8.x,b_p8.y), 3, Scalar(0,0,255), -1, 8, 0);
		*/

	    stringstream wynik_lewy_str, wynik_prawy_str;
	    string wl, wp;
	    
	    wynik_lewy_str << wynik_lewy;
	    wynik_prawy_str << wynik_prawy;
	    wl = wynik_lewy_str.str();
	    wp = wynik_prawy_str.str();
	    
	    
        // rysowanie boiska
		line(bramki,Point(window_size_x/2, 0),Point(window_size_x/2,window_size_y),Scalar(255,255,255),2,8,0); // polowka "boiska"	
		
        rectangle(bramki, Point(bramka_lewa.position.x,bramka_lewa.position.y), Point(bramka_lewa.position.x+bramka_lewa.width,bramka_lewa.position.y+bramka_lewa.height), Scalar(255,255,255), -1, 8); // bramka lewa
        rectangle(bramki, Point(bramka_prawa.position.x,bramka_prawa.position.y), Point(bramka_prawa.position.x+bramka_prawa.width,bramka_prawa.position.y+bramka_prawa.height), Scalar(255,255,255), -1, 8); // bramka prawa
        		      
        // pileczka
        circle(bramki, Point(ball.center.x,ball.center.y), ball.r, Scalar(255,255,0), -1, 8, 0);
        
        putText( bramki, wl, Point(10, window_size_y-10), CV_FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 2, 8 ); // wynik lewy
        putText( bramki, wp, Point(window_size_x-20, window_size_y-10), CV_FONT_HERSHEY_COMPLEX, 0.6, Scalar(0, 255, 0), 2, 8 ); // wynik prawy
     
        addWeighted(bramki,0.6,original_img,1, 0, original_img);
        
        imshow(ORIGINAL_WINDOW, original_img);
        
        //imshow("Kontury", drawing); 
        imshow("Gra",bramki);
		imshow(THRESHOLD_WINDOW, threshold_img);
        //*** 
		
			
		if(waitKey(30)==113) {
			cout << "Kończymy działanie programu." << endl;
			break;
			}	
		}
	
	camera.release();
	return 0;
}

